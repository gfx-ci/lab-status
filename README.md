# lab-status

Configuration file with the current status of the different labs that provide HW for testing.

Pipelines that use https://gitlab.freedesktop.org/gfx-ci/drm-ci/ will include this file and won't attempt to run jobs for DUTs that are hosted in labs that are offline at the moment.
